


import sys, numpy
from itertools import permutations, combinations

# remember, board[y][x] is how to index!!!
class Board():
    """WWF Board"""
    def __init__(self, status_string):
        self.width = 8
        self.height = 10
        self.board = [[0]*self.width]*self.height
        self.surface = [[0 for i in range(self.width)] for j in range(self.height)]

    def get_column_envs(col):
        """Returns a list of Env objects which are for testing tile patterns"""
        return [Environment(self, i) for i in len(board)]

    def __str__(self):
        return "\n".join([" ".join([str(x).upper() if x else '-' for x in row]) for row in self.surface])

    def get_horizontal_word(self, row, column):
        """Returns the rest of a word given a spot at the beginning or end
        
        Returns an empty list if no word there
        WARNING: Misbehaves if started in middle of word!"""
        row = row
        column = column
        word = []

    def __getitem__(self, i):
        return self.board[i]

    def __setitem__(self, *i):
        self.board[i[0]][i[1]] = value


class Environment():
    """Describes a row or column for placing tiles"""
    def __init__(self, board, column):
        # need to describe constraints
        s = board.surface
        constraints = {}
        for row in range(board.height):
            if s[row][column] != 0:
                constraints[row] = s[row][column]
                continue
            if column != 0:
                if s[row-1][column] != 0:
                    pass
            if column != board.width-1:
                if s[row+1][column] != 0:
                    pass
                    
    def get_horizontal_contraint(self, blank_row, blank_column):
        "Returns a string describing a space with a ? where blank is"
        row = row
        column = column
        word = ['?']
        s = self.surface
        while True:
            # look forward first
            row += 1
            if s[row][column]:
                word.append(s[row][column])
                if row > self.width-1:
                    break
            else:
                break
        # next look backwards!
        while True:
            if self.surface[row][column]:
                word.prepend()




    def __str__(self):
        return self.constraints
        




if __name__ == '__main__':
    s = open('/projects/schiz/ra/tomb/wwf/ryan2.PNG.code').read().replace('\n','')
    board = Board(s)
    board.surface[3][3] = 'd'
    board.surface[3][4] = 'a'
    board.surface[3][5] = 'd'
    board.surface[6][6] = 'i'
    board.surface[6][7] = 'A'
    board[3][7] = 'B'
    print board
    #print board.get_horizontal_word(3,3)
