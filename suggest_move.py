#!/usr/bin/env python

import sys
import numpy as np
import string
import pp
from itertools import permutations
from itertools import combinations


class Game:

    _points = {
        'a':1,
        'b':4,
        'c':4,
        'd':2,
        'e':1,
        'f':4,
        'g':3,
        'h':3,
        'i':1,
        'j':10,
        'k':5, 
        'l':2,
        'm':4,
        'n':2,
        'o':1,
        'p':2, #check
        'q':8, #check
        'r':1,
        's':1,
        't':1,
        'u':2,
        'v':5, #check
        'w':5, #check
        'x':8,
        'y':3,
        'z':8, #check
    }

    def _read_file_into_list(self, file):
        list = []
        with open(file) as f:
            for line in f:
                list.append(line.rstrip())
        return list

    def __init__(self, board_file, letters):
        self._board = self._load_board(board_file)
        #: we sort the letters to avoid 'unique' subsets 
        #: of letters that are actually the same, 
        #: e.g. ('s','r','b','a') and ('s','b','r','a')
        self._letters = sorted(letters)         
        self._dictionary = self._read_file_into_list('dictionary.txt')

    def _string_to_ascii(self, str):
        return [ord(char) for char in str]

    def _load_board(self, board_file):
        list = []
        with open(board_file) as f:
            for line in f:
                list.append(self._string_to_ascii(line))
        return np.array(list)

    def print_board(self):
        for row in self._board:
            for entry in row:
                sys.stdout.write(chr(entry))
            print ""

    def _is_letter(self,ascii_code):
        return ascii_code > 96 and ascii_code < 123

    def _letter_above(self, row, col):
        if row == 0 or not self._is_letter(self._board[row-1,col]):
            return False
        return True

    def _letter_below(self, row, col):
        if row == self._board.shape[0]-1:
            return False
        if self._is_letter(self._board[row+1,col]):
            return True
        return False

    def _print_vector(self,vector):
        print  ''.join([chr(ascii) for ascii in vector]),

    def _free_spaces_vector(self,vector):
        count = 0
        found_letter = False

        for i in range(vector.shape[0]-2):
            if self._is_letter(vector[i+2]):
                found_letter = True
                break
            count += 1

        if not found_letter and not self._is_letter(vector[-1]):
            count +=1
            
        return count

    def _free_spaces_below(self, row, col):
        vector = self._board[row:,col]
        #_print_vector(vector) 
        #print 'free spaces: ' + str(free_spaces_vector(vector))
        return self._free_spaces_vector(vector)

    def _free_spaces_above(self, row, col):
        vector = self._board[row::-1,col]
        #_print_vector(vector) 
        #print 'free spaces: ' + str(free_spaces_vector(vector))
        return self._free_spaces_vector(vector)

    def _vertical_free_spaces(self, row, col):
        if self._letter_below(row, col) or self._letter_above(row, col):
            return [0,0]
        return [self._free_spaces_above(row,col), self._free_spaces_below(row,col)]

    def get_letter_multiplier(self, row, col):
        board = self._board
        if board[row,col] == ord('2'):
            return 2
        if board[row,col] == ord('3'):
            return 3
        return 1

    def get_word_multiplier(self, row, col):
        board = self._board
        if board[row,col] == ord('4'):
            return 2
        if board[row,col] == ord('6'):
            return 3
        return 1

    def _score(self, word, row, col, orientation):
        curr_row = row
        curr_col = col
        score = 0
        word_multiplier = 1
        for letter in word:
            points = self._points[letter]
            if self._is_letter(self._board[curr_row, curr_col]):
                assert(self._board[curr_row, curr_col] == ord(letter))
                score += points
            else:
                letter_multiplier = self.get_letter_multiplier(curr_row, curr_col)
                word_multiplier = max(self.get_word_multiplier(curr_row, curr_col), word_multiplier)
                score +=  letter_multiplier * points
            #print letter + ' ' + str(points) + ' ' + str(curr_row) + ',' + str(curr_col)
            if orientation == 'horizontal':
                curr_col +=1
            elif orientation == 'vertical':
                curr_row +=1
            else:
                raise Exception('direction must be horizontal or vertical')
        
        #print 'word m is ' + str(word_multiplier) + ' and total score is ' + str(score * word_multiplier)
        return score * word_multiplier

    def _words(self, row, col, free_above, free_below):
        max_space = max(free_above, free_below)
        letter = chr(self._board[row,col])
        result = []
        make_words_above = True
        make_words_below = True
        for size in range(1, min(max_space+1, 8) ):
            if free_above < size:
                make_words_above = False
            if free_below < size:
                make_words_below = False
            print 'Considering all permuations of each unique subset of letters of size ' + str(size),
            for subset in set(combinations(self._letters,size)):
                #print 'considering letters subset: ' + str(subset)
                partials = [''.join(perm) for perm in list(permutations(subset))]
                for partial in partials:
                    if make_words_above:
                        a_word = partial + letter
                        if a_word in self._dictionary:
                            result.append(a_word)
                            print 'a(' + a_word + ' ' + str(self._score(a_word,row-size,col,'vertical')) + ')',
                    if make_words_below:
                        a_word = letter + partial
                        if a_word in self._dictionary:
                            result.append(a_word)
                            print 'b(' + a_word + ' ' + str(self._score(a_word,row,col,'vertical')) + ')',

            print ""
        return result

    def empty(self, row, col):
        return self._board[row,col] <  97 or self._board[row,col] > 122

    def outside(self, row, col):
        return row >= self._board.shape[0] or col >= self._board.shape[1]

    def board(self, row, col):
        return chr(self._board[row,col])

    def get_hword(self,row,col):
        if self.empty(row,col):
            return ''
        part1 = ''
        part2 = ''
        cur_col = col+1

        while not self.outside(row, cur_col) and not self.empty(row, cur_col):
            part2 += self.board(row,cur_col)
            cur_col += 1

        cur_col = col-1
        while not self.outside(row, cur_col) and not self.empty(row, cur_col):
            part1 = self.board(row,cur_col) + part1
            cur_col -= 1

        return part1 + self.board(row,col) + part2

    def _str(self, row, col):
        return '(' + str(row) + ',' + str(col) + ')'

    def rows(self):
        return self._board.shape[0]

    def cols(self):
        return self._board.shape[1]

    def suggest_move(self):
        board = self._board
        for row in range(self.rows()):
            for col in range(self.cols()):
                if not self.empty(row,col):
                    fs = self._vertical_free_spaces(row,col)
                    if fs[0] > 0 or fs[1] > 0:
                        print 'Considering options at ' + str(row) + ',' + str(col) + ' ' 
                        word_list =  self._words(row, col, fs[0], fs[1])
                elif not self.empty(row,col-1) and self.empty(row,col+1) and self.empty(row,col+2):
                    hword = self.get_hword(row,col)
                    for letter in self._letters:
                        if hword + letter in self._dictionary:
                            print(hword + letter)

                    #self.get_hword(row,col)
                    #isword = [word in self._dictionary for 

        print ""
        self.print_board()

def main():
    game = Game(sys.argv[1], sys.argv[2])
    game.suggest_move()
    #suggest_move(board(sys.argv[1]), sys.argv[2], dictionary('dictionary.txt'));
    #print_board(board)


if __name__ == '__main__':
    #import cProfile
    #cProfile.run('main()', 'prof')
    main()
