#!/usr/bin/env python

import sys
import numpy as np
import string
from itertools import permutations
from itertools import combinations

#BOARD = """
#* |* |* |TW|* |* |TL|* | 
#* |* |DL|* |* |DW|* |* |
#* |DL|* |* |DL|* |* |* |
#TW|* |* |TL|* |* |* |DW|
#* |* |DL|* |* |* |DL|* |
#* |DW|* |* |* |TL|* |* |
#TL|* |* |* |DL|* |* |* |
#* |* |* |DW|* |* |* |0 |
#"""

quad = np.array([
    [ 0,0,0,6,0,0,3,0 ],
    [ 0,0,2,0,0,4,0,0 ],
    [ 0,2,0,0,2,0,0,0 ],
    [ 6,0,0,3,0,0,0,4 ],
    [ 0,0,2,0,0,0,2,0 ],
    [ 0,4,0,0,0,3,0,0 ],
    [ 3,0,0,0,2,0,0,0 ],
    [ 0,0,0,4,0,0,0,0 ]
])

def create_board():
    quad1 = quad[:-1,:-1]
    quad3 = quad[::-1,:-1]
    quad2 =  quad[:-1,::-1]
    quad4 = quad[::-1,::-1]
    board = np.vstack( [ np.hstack([quad1, quad2]), np.hstack([quad3, quad4]) ] )


def print_board(board):
    for row in board:
        for entry in row:
            sys.stdout.write(chr(entry))
        print ""


def string_to_ascii(str):
    return [ord(char) for char in str]
    

def board(board_file):
    list = []
    with open(board_file) as f:
        for line in f:
            list.append(string_to_ascii(line))
    return np.array(list)


def dictionary(dictionary_file):
    list = []
    with open(dictionary_file) as f:
        for line in f:
            list.append(line.rstrip())
    return list


def is_letter(ascii_code):
    return ascii_code > 96 and ascii_code < 123


def letter_above(board, row, col):
    if row == 0 or not is_letter(board[row-1,col]):
        return False
    return True

def letter_below(board, row, col):
    if row == board.shape[0]-1:
        return False
    if is_letter(board[row+1,col]):
        return True
    return False


def print_vector(vector):
    #print  str(row) + ',' + str(col) + ' ' + ''.join([chr(ascii) for ascii in vector]) + ' ' + str(count) + ' ' + str(letter_below(board, row, col))
    print  ''.join([chr(ascii) for ascii in vector]),

def free_spaces_vector(vector):
    count = 0
    found_letter = False

    for i in range(vector.shape[0]-2):
        if is_letter(vector[i+2]):
            found_letter = True
            break
        count += 1

    if not found_letter and not is_letter(vector[-1]):
        count +=1
        
    return count
    
def free_spaces_below(board, row, col):
    vector = board[row:,col]
    #print_vector(vector) 
    #print 'free spaces: ' + str(free_spaces_vector(vector))
    return free_spaces_vector(vector)

def free_spaces_above(board, row, col):
    vector = board[row::-1,col]
    #print_vector(vector) 
    #print 'free spaces: ' + str(free_spaces_vector(vector))
    return free_spaces_vector(vector)

def vertical_free_spaces(board, row, col):
    if letter_below(board, row, col) or letter_above(board, row, col):
        return [0,0]
    return [free_spaces_above(board,row,col), free_spaces_below(board,row,col)]


def word(letter, letters, free_above, free_below, dictionary):
    words = []
    for size in range(1,free_below+1):
        for subset in list(combinations(letters,size)):
            partials = [''.join(perm) for perm in list(permutations(subset))]
            for partial in partials:
                a_word = letter + partial
                if a_word in dictionary:
                    words.append(a_word)

    #for size in range(1,free_above+1):
        #for subset in list(combinations(letters,size)):
            #partials = [''.join(perm) for perm in list(permutations(subset))]
            #for partial in partials:
                #a_word = partial + letter
                #if a_word in dictionary:
                    #words.append(a_word)

    return words

def suggest_move(board, letters, dictionary):
    for row in range(board.shape[0]):
        for col in range(board.shape[1]):
            if is_letter( board[ row,col ] ):
                fs = vertical_free_spaces(board,row,col)
                if fs[0] > 0 or fs[1] > 0:
                    print str(row) + ',' + str(col) + ' ', 
                    print word(chr(board[row,col]), letters, fs[0], fs[1], dictionary)
    print ""
    print_board( board )
    print letters


def main():
    suggest_move(board(sys.argv[1]), sys.argv[2], dictionary('dictionary.txt'));
    #print_board(board)


if __name__ == '__main__':
    import cProfile
    cProfile.run('main()')
